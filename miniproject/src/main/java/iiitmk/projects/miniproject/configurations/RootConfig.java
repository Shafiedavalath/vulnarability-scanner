package iiitmk.projects.miniproject.configurations;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.stereotype.Repository;

/*@Configuration
@ComponentScan(basePackages = { "iiitmk.projects.miniproject" }, excludeFilters = {
		@Filter(type = FilterType.ANNOTATION, value = EnableWebMvc.class),
		@Filter(type = FilterType.ANNOTATION, value = Controller.class) }, includeFilters = {
				@Filter(type = FilterType.ANNOTATION, value = Service.class),
				@Filter(type = FilterType.ANNOTATION, value = Repository.class) })
public class RootConfig {
}*/