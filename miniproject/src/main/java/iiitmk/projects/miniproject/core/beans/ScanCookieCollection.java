package iiitmk.projects.miniproject.core.beans;

public class ScanCookieCollection {
	private int mappingId;
	private int scanId;
	private String cookieName;
	private String cookieVallue;

	public int getMappingId() {
		return mappingId;
	}

	public void setMappingId(int mappingId) {
		this.mappingId = mappingId;
	}

	public int getScanId() {
		return scanId;
	}

	public void setScanId(int scanId) {
		this.scanId = scanId;
	}

	public String getCookieName() {
		return cookieName;
	}

	public void setCookieName(String cookieName) {
		this.cookieName = cookieName;
	}

	public String getCookieVallue() {
		return cookieVallue;
	}

	public void setCookieVallue(String cookieVallue) {
		this.cookieVallue = cookieVallue;
	}

}
