package iiitmk.projects.miniproject.core.beans;

public class ScanRecordDetailed {
	private int scanRecordId;
	private int testId;
	private String severity;

	public int getScanRecordId() {
		return scanRecordId;
	}

	public void setScanRecordId(int scanRecordId) {
		this.scanRecordId = scanRecordId;
	}

	public int getTestId() {
		return testId;
	}

	public void setTestId(int testId) {
		this.testId = testId;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

}
