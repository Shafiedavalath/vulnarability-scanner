package iiitmk.projects.miniproject.core.services;

import iiitmk.projects.miniproject.core.beans.Role;
import iiitmk.projects.miniproject.core.beans.User;

public interface UserService {

	User findById(int id);	
	User findByName(String userName);
	Role getUserRole(int roleId);
	
}