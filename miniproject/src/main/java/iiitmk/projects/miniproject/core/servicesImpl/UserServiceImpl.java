package iiitmk.projects.miniproject.core.servicesImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import iiitmk.projects.miniproject.core.beans.Role;
import iiitmk.projects.miniproject.core.beans.User;
import iiitmk.projects.miniproject.core.dao.RoleDao;
import iiitmk.projects.miniproject.core.dao.UserDao;
import iiitmk.projects.miniproject.core.services.UserService;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private RoleDao roleDao;

	public User findById(int id) {
		return userDao.findById(id);
	}

	public User findByName(String sso) {
		return userDao.findByName(sso);
	}

	@Override
	public Role getUserRole(int roleId) {		
		return roleDao.findById(roleId);
	}

}
