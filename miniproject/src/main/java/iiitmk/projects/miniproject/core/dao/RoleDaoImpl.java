package iiitmk.projects.miniproject.core.dao;

import org.springframework.stereotype.Repository;

import iiitmk.projects.miniproject.core.beans.Role;

@Repository("roleDao")
public class RoleDaoImpl extends AbstractDao<Integer, Role> implements RoleDao {

	@Override
	public Role findById(int roleId) {
		return getByKey(roleId);
	}
}
