package iiitmk.projects.miniproject.dao.daoimpl;

import org.springframework.stereotype.Repository;

import iiitmk.projects.miniproject.beans.ScanRecordDetailed;
import iiitmk.projects.miniproject.dao.AbstractDao;
import iiitmk.projects.miniproject.dao.DetailedRecordDao;

@Repository("detailedRecordDao")
public class DetailedRecordDaoImpl extends AbstractDao<Integer, ScanRecordDetailed> implements DetailedRecordDao{

	@Override
	public ScanRecordDetailed getDetailedRecordById(Integer key) {
		
		return getByKey(key);
	}

	@Override
	public Integer insertDetailedScanRecord(ScanRecordDetailed scanRecord) {

		return save(scanRecord);
	}

}
