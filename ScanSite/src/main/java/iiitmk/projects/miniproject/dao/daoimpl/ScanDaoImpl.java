package iiitmk.projects.miniproject.dao.daoimpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import iiitmk.projects.miniproject.beans.ScanRecordAbstract;
import iiitmk.projects.miniproject.beans.ScanSettings;
import iiitmk.projects.miniproject.beans.User;
import iiitmk.projects.miniproject.dao.AbstractDao;
import iiitmk.projects.miniproject.dao.ScanDao;

@Repository("scanDao")
public class ScanDaoImpl extends AbstractDao<Integer, ScanSettings> implements ScanDao {
	public ScanSettings findByScanId(int Scanid) {
		return getByKey(Scanid);

	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<ScanSettings> getAllScanId() {
		Criteria crit = createEntityCriteria();
		return (ArrayList<ScanSettings>) crit.list();
	}

	@Override
	public void updateScanStatus(int scanId, String scanStatus, int percentageComplete, Boolean isActive, Integer recordPk) {
		ScanSettings scan = getByKey(scanId);
		scan.setStatusText(scanStatus);
		scan.setIsActive(isActive);
		scan.setPercentCompleted(percentageComplete);
		scan.setActiveScanRecordId(recordPk);
		merge(scan);
	}

	@Override
	public Integer insertNewScan(ScanSettings scanSettings) {
		return save(scanSettings);

	}

	public Collection<ScanSettings> getInactiveScansByUsername(String userName) {
		Criteria crit = createEntityCriteria();
		crit.createAlias("user", "user").add(Restrictions.eq("user.userName", userName));
		crit.add(Restrictions.eq("isActive", false));
		Collection<ScanSettings> inactiveScansOfUser = crit.list();

		if (inactiveScansOfUser != null) {
			return inactiveScansOfUser;
		} else {
			return null;
		}
	}

	@Override
	public ScanSettings getActiveScansByUsername(String userName) {
		Criteria crit = createEntityCriteria();
		crit.createAlias("user", "user").add(Restrictions.eq("user.userName", userName));
		crit.add(Restrictions.eq("isActive", true));
		ScanSettings activeScansOfUser = (ScanSettings) crit.uniqueResult();

		if (activeScansOfUser != null) {

			return activeScansOfUser;
		} else {
			return null;
		}
	}

	@Override
	public Collection<ScanRecordAbstract> getAbstractRecords(Integer scanId) {
				
		return getByKey(scanId).getScanRecords();
	}

}
