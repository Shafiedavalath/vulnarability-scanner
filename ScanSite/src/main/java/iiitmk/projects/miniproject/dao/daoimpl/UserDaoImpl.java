package iiitmk.projects.miniproject.dao.daoimpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import iiitmk.projects.miniproject.beans.ScanSettings;
import iiitmk.projects.miniproject.beans.User;
import iiitmk.projects.miniproject.dao.AbstractDao;
import iiitmk.projects.miniproject.dao.UserDao;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	public User findById(int id) {
		return getByKey(id);
	}

	public User findBySSO(String sso) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("userName", sso));
		return (User) crit.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<User> getAllUsers() {
		Criteria crit = createEntityCriteria();
		return (ArrayList<User>) crit.list();
	}

	@Override
	public void addUser(User user) {
		// TODO Auto-generated method stub
		persist(user);
	}	
	
	
}
