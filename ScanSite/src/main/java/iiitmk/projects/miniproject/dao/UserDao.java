package iiitmk.projects.miniproject.dao;

import java.util.Collection;

import iiitmk.projects.miniproject.beans.ScanSettings;
import iiitmk.projects.miniproject.beans.User;

public interface UserDao {

	User findById(int id);	
	User findBySSO(String sso);
	Collection<User> getAllUsers();
	void addUser(User user);
}

