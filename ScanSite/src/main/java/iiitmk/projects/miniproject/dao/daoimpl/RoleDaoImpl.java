package iiitmk.projects.miniproject.dao.daoimpl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import iiitmk.projects.miniproject.beans.Role;
import iiitmk.projects.miniproject.beans.User;
import iiitmk.projects.miniproject.dao.AbstractDao;
import iiitmk.projects.miniproject.dao.RoleDao;

@Repository("roleDao")
public class RoleDaoImpl extends AbstractDao<Integer, Role> implements RoleDao {

	public Role findById(int roleId) {
		return getByKey(roleId);
	}
	public Role findByName(String roleName){
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("type", roleName));
		return (Role) crit.uniqueResult();
	}
}
