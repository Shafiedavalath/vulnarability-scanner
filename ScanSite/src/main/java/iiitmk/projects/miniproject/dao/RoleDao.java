package iiitmk.projects.miniproject.dao;

import iiitmk.projects.miniproject.beans.Role;

public interface RoleDao {
 	Role findById(int roleId);
 	Role findByName(String roleName);
}
