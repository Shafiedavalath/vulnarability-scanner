package iiitmk.projects.miniproject.dao;

import java.util.List;

import iiitmk.projects.miniproject.beans.Plugin;
import iiitmk.projects.miniproject.beans.PluginTest;

public interface PluginDao {
	void addPlugin(Plugin plugin);

	List<PluginTest> getAllPluginTest();
}
