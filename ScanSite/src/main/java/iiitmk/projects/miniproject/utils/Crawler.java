package iiitmk.projects.miniproject.utils;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URI;
import java.net.URISyntaxException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import iiitmk.projects.miniproject.beans.ScanCookieCollection;
import iiitmk.projects.miniproject.beans.ScanUrlCollection;

/**
 * Example program to list links from a URL.
 */
public class Crawler {
	private int depth;
	private String userAgent;
	private List<ScanCookieCollection> cookies;
	private String hostName;

	public Crawler() {
		this.depth = 3;
		this.userAgent = "Mozilla";
	}

	public List<ScanUrlCollection> crawl(String url) {

		Set<ScanUrlCollection> outputUrlList = new HashSet<ScanUrlCollection>();

		List<ScanUrlCollection> workingUrlList = new ArrayList<ScanUrlCollection>();
		ScanUrlCollection primaryUrl = new ScanUrlCollection();			

		try {
			
			this.setHostName(new URI(url).getHost());
			primaryUrl.setCreatedOn(new Date());
			primaryUrl.setHasParameter(hasParameter(url));
			primaryUrl.setIsSelected(false);
			primaryUrl.setUrl(url);
			outputUrlList.add(primaryUrl);
			workingUrlList.add(primaryUrl);
			
			for (int i = 0; i < this.depth; i++) {
				Set<ScanUrlCollection> tempUrlList = new HashSet<ScanUrlCollection>();
				for (int j = 0; j < workingUrlList.size(); j++) {
					//System.out.println(hostName);
					
					Document doc = getDocument(workingUrlList.get(j).getUrl());
					if(doc == null){
						continue;
					}
					Elements links = doc.select("a[href]");
					Elements forms = doc.select("form");

					for (Element link : links) {
						if(link.attr("abs:href").contains(this.hostName)){
							ScanUrlCollection scanUrl = new ScanUrlCollection();
							scanUrl.setCreatedOn(new Date());
							scanUrl.setHasParameter(hasParameter(link.attr("abs:href")));
							scanUrl.setIsSelected(false);
							scanUrl.setUrl(link.attr("abs:href"));
							tempUrlList.add(scanUrl);
						}
					
					}

					for (Element form : forms) {
						
						if(form.attr("abs:action").contains(this.hostName)){
							Elements inputs = form.children().select("input[type=text]");
							String tempLink = form.attr("abs:action") + "?";
							for (Element input : inputs) {
								tempLink += input.attr("name") + "=id&";
							}

							ScanUrlCollection scanUrl = new ScanUrlCollection();
							scanUrl.setCreatedOn(new Date());
							scanUrl.setHasParameter(true);
							scanUrl.setIsSelected(true);
							scanUrl.setUrl(tempLink);
							tempUrlList.add(scanUrl);
						}
						
					}

				}
				tempUrlList.removeAll(outputUrlList);
				outputUrlList.addAll(tempUrlList);
				workingUrlList = new ArrayList<ScanUrlCollection>();
				workingUrlList.addAll(tempUrlList);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ArrayList<ScanUrlCollection>(outputUrlList);
	}

	private Document getDocument(String url) {
		
		Document doc;
		try {
			Map<String, String> cookiesMap = new HashMap<String, String>();
		System.out.println("url===" + url);
		if (this.cookies != null) {
			for (ScanCookieCollection scan : this.cookies) {
				cookiesMap.put(scan.getCookieName(), scan.getCookieVallue());
			}
			doc = Jsoup.connect(url).userAgent(this.userAgent).cookies(cookiesMap).get();
		} else {
			doc = Jsoup.connect(url).userAgent(this.userAgent).get();
		}
		} catch (Exception e) {
			e.printStackTrace();
			doc = null;
		}
		return doc;		
	}

	private Boolean hasParameter(String url) throws URISyntaxException {
		String query = new URI(url).getQuery();
		//System.out.println(query);
		return query != null && !query.isEmpty();
	}

	public static void main(String[] args) throws IOException, URISyntaxException {
		Crawler crawler = new Crawler();
		List<ScanUrlCollection> outputUrlList = new ArrayList<ScanUrlCollection>();
		outputUrlList = crawler.crawl("http://google-gruyere.appspot.com/138191955499/");
		
		System.out.println("has parameter : " + crawler.hasParameter("http://google-gruyere.appspot.com/138191955499/"));
		
		for(ScanUrlCollection url: outputUrlList){
			System.out.println(url.getUrl());
		}
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public List<ScanCookieCollection> getCookies() {
		return cookies;
	}

	public void setCookies(List<ScanCookieCollection> cookies) {
		this.cookies = cookies;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	
	
	
}
