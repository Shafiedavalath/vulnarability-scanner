package iiitmk.projects.miniproject.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * @author vipin
 *
 */
public class ProcessExecuter extends Thread {
	InputStream is;
	OutputStream os;
	Boolean isSuccess = false;
	String command;
	String successCondition;
	String conditionType;
	StringBuilder output;
	
	public ProcessExecuter(InputStream is, OutputStream os, String successCondition) {
		this.is = is;
		this.os = os;
		this.successCondition = successCondition;
	}
	public ProcessExecuter() {
	}

	public void run() {
		try {
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			this.output = new StringBuilder();
			int singleChar;
			while ((singleChar = br.read()) != -1) {

				output.append(Character.toChars(singleChar));
				System.out.print(Character.toChars(singleChar));

				// successCondition				
				if (output.indexOf(this.successCondition) != -1) {
					System.out.println("-----------------is success");
					System.out.println(output);
					this.isSuccess = true;
				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}


	//this method return true if the test is success(not vulnerable) otherwise false
	public Boolean executeProcess() {
		ProcessExecuter errorGobbler = null;
		ProcessExecuter outputGobbler = null;
		
		try {
			
			Runtime rt = Runtime.getRuntime();
			System.out.println(this.command);
			Process proc = rt.exec(this.command);
			errorGobbler = new ProcessExecuter(proc.getErrorStream(), proc.getOutputStream(), this.successCondition);

			// any output?
			outputGobbler = new ProcessExecuter(proc.getInputStream(), proc.getOutputStream(), this.successCondition);

			// kick them off
			outputGobbler.start();
			errorGobbler.start();
			
			outputGobbler.join();
			errorGobbler.join();
			proc.waitFor();			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(" is success - output glober: " + outputGobbler.isSuccess.toString());
		System.out.println(" is success - error glober: " + errorGobbler.isSuccess.toString());
		
		String[] outputs = outputGobbler.output.toString().split("\n");
		if(outputs[outputs.length-1].contains("CRITICAL")){
			return true;
		}
		
		return outputGobbler.isSuccess || errorGobbler.isSuccess;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getSuccessCondition() {
		return successCondition;
	}
	public void setSuccessCondition(String successCondition) {
		this.successCondition = successCondition;
	}
	
	public static void main(String args){
		
	}
}
