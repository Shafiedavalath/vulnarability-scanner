package iiitmk.projects.miniproject.service.serviceimpl;

import java.sql.Date;
import java.util.Collection;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import iiitmk.projects.miniproject.beans.PluginTest;
import iiitmk.projects.miniproject.beans.ScanRecordAbstract;
import iiitmk.projects.miniproject.beans.ScanRecordDetailed;
import iiitmk.projects.miniproject.beans.ScanSettings;
import iiitmk.projects.miniproject.beans.ScanUrlCollection;
import iiitmk.projects.miniproject.dao.DetailedRecordDao;
import iiitmk.projects.miniproject.dao.ScanDao;
import iiitmk.projects.miniproject.dao.UserDao;
import iiitmk.projects.miniproject.service.ScanService;
import iiitmk.projects.miniproject.utils.Crawler;

@Service("scanService")
@Transactional
public class ScanServiceImpl implements ScanService{
	@Autowired
	ScanDao scanDao;
	@Autowired
	UserDao userDao;
	
	@Override
	public ScanSettings getScanById(Integer scanId) {		
		return scanDao.findByScanId(scanId);
	}

	@Override
	public Collection<ScanSettings> getInactiveScansByUsername(String userName) {
		// TODO Auto-generated method stub
		return scanDao.getInactiveScansByUsername(userName);
	}

	@Override
	public ScanSettings getActiveScansByUsername(String userName) {
		// TODO Auto-generated method stub
		return scanDao.getActiveScansByUsername(userName);
	}

	@Override
	public void updateScanStatus(int scanId, String scanStatus, int percentageComplete, Boolean isActive, Integer recordPk) {
		scanDao.updateScanStatus(scanId, scanStatus, percentageComplete, isActive, recordPk);
		
	}

	@Override
	public JSONArray getCrawledUrls(String primaryUrl) {
		JSONArray urlJsonArray = new JSONArray();
		Crawler crawler = new Crawler();
		List<ScanUrlCollection> scanUrlList =  crawler.crawl(primaryUrl);
		
		for(ScanUrlCollection url : scanUrlList){
			JSONObject urlObj = new JSONObject();
			urlObj.put("url", url.getUrl());
			urlObj.put("hasParameter", url.getHasParameter());
			urlJsonArray.put(urlObj);
		}
		
		return urlJsonArray;
	}

	@Override
	public Integer insertNewScan(ScanSettings scanSettings) {
		ScanSettings newScan = new ScanSettings();
		
		newScan.setCreatedOn(new Date(new java.util.Date().getTime()));
		newScan.setHasScheduledScan(false);
		newScan.setIsActive(false);
		newScan.setPrimaryUrl(scanSettings.getPrimaryUrl());
		newScan.setUser(userDao.findBySSO(getPrincipal()));
		newScan.setScanType(scanSettings.getScanType());
		newScan.setUserAgent("Mozilla");
		Collection<ScanUrlCollection> scanUrls = scanSettings.getListOfUrls();
		Collection<PluginTest> tests = scanSettings.getTests();
		
		for(ScanUrlCollection url : scanUrls){
			url.setScanSettings(newScan);
			newScan.getListOfUrls().add(url);
		}
		
		for(PluginTest test : tests){
			newScan.getTests().add(test);
		}
		
		return scanDao.insertNewScan(newScan);
		
	}
	
	private String getPrincipal(){
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}

	@Override
	public Collection<ScanRecordAbstract> getAbstractRecords(Integer scanId) {

		return scanDao.getAbstractRecords(scanId);
	}
	
}
