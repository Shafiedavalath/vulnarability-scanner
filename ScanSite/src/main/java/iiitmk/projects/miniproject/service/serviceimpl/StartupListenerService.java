package iiitmk.projects.miniproject.service.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import iiitmk.projects.miniproject.service.PluginService;
import iiitmk.projects.miniproject.service.RecordService;
import iiitmk.projects.miniproject.service.ScanService;
import iiitmk.projects.miniproject.utils.GoodWindowsExec;
import iiitmk.projects.miniproject.utils.ScanScheduler;

@Component
public class StartupListenerService  implements ApplicationListener<ContextRefreshedEvent> {
	
	@Autowired
	PluginService pluginService;
	
	@Autowired
	ScanService scanService;
	
	@Autowired
	RecordService recordService;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		
		//load plugins on startup
		if(!arg0.getApplicationContext().getId().contains("dispatcher")){
			pluginService.loadPlugins();
			//GoodWindowsExec ex = new GoodWindowsExec();
//			ex.runMain();			
			ScanScheduler scheduler = new ScanScheduler();
			scheduler.setScanSettings(scanService.getScanById(3));
			scheduler.setRecordService(recordService);
			scheduler.setScanService(scanService);
			//scheduler.executeTaskT();
		}
		
		
	}

}
