package iiitmk.projects.miniproject.service;

import iiitmk.projects.miniproject.beans.User;

import java.util.Collection;

import iiitmk.projects.miniproject.beans.Role;
import iiitmk.projects.miniproject.beans.ScanSettings;

public interface UserService {

	User findById(int id);	
	User findByUsename(String sso);
	Role getUserRole(int roleId);
	Collection<User> getAllUsers(); 
	void addUser(String user,String password,String roleName);
	
}