package iiitmk.projects.miniproject.beans;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.JoinColumn;

@Entity
@Table(name="scan_settings")
public class ScanSettings {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "scan_id")
	private int scanId;
	
	@Column(name="primary_url", nullable = false)
	private String primaryUrl;
	
	@Column(name="user_agent")
	private String userAgent;
	
	@Column(name="scan_type", nullable = false)
	private String scanType;
	
	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="has_scheduled_scan")
	private Boolean hasScheduledScan;
	
	@Column(name="created_on")
	private Date createdOn;
	
	@Column(name="last_scanned")
	private Date lastScanned;
	
	@Column(name="status_text")
	private String statusText;
	
	@Column(name="percent_completed")
	private Integer percentCompleted;
	
	@Column(name="active_scan_record_id")
	private Integer activeScanRecordId;
	
	/*@ElementCollection(fetch = FetchType.EAGER)
	@JoinTable(name="scan_url_collection", joinColumns=@JoinColumn(name="scan_id"))
	@GenericGenerator(name="sequence-gen1",strategy="sequence")
	@CollectionId(columns={@Column(name="mapping_id")}, generator="sequence-gen1",type=@Type(type="int"))*/
	
	@OneToMany(mappedBy = "scanSettings",cascade = CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)

	private Collection<ScanUrlCollection> listOfUrls = new ArrayList<ScanUrlCollection>();
	
/*	@ElementCollection(fetch = FetchType.EAGER)
	@JoinTable(name="scan_cookie_collection", joinColumns=@JoinColumn(name="scan_id"))
	@GenericGenerator(name="sequence-gen2",strategy="sequence")
	@CollectionId(columns={@Column(name="mapping_id")}, generator="sequence-gen2",type=@Type(type="int"))*/
	
	@OneToMany(mappedBy = "scanSettings", cascade = CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)

	private Collection<ScanCookieCollection> listOfCookies = new ArrayList<ScanCookieCollection>();
	
	@ManyToMany
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(name = "scan_test_mapping",joinColumns = { 
			@JoinColumn(name = "scan_id", nullable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "test_id", 
					nullable = false) })	
	private Collection<PluginTest> tests = new ArrayList<PluginTest>();
	
	@OneToMany(mappedBy="scan")	
	private Collection<ScanRecordAbstract> scanRecords = new ArrayList<ScanRecordAbstract>();
	
	@ManyToOne
	@JoinTable(name = "user_scan_mappings", joinColumns = { @JoinColumn(name = "scan_id") }, inverseJoinColumns = {
			@JoinColumn(name = "user_id") })
	private User user ;

	public int getScanId() {
		return scanId;
	}

	public void setScanId(int scanId) {
		this.scanId = scanId;
	}

	public String getPrimaryUrl() {
		return primaryUrl;
	}

	public void setPrimaryUrl(String primaryUrl) {
		this.primaryUrl = primaryUrl;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getScanType() {
		return scanType;
	}

	public void setScanType(String scanType) {
		this.scanType = scanType;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getHasScheduledScan() {
		return hasScheduledScan;
	}

	public void setHasScheduledScan(Boolean hasScheduledScan) {
		this.hasScheduledScan = hasScheduledScan;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getLastScanned() {
		return lastScanned;
	}

	public void setLastScanned(Date lastScanned) {
		this.lastScanned = lastScanned;
	}

	public String getStatusText() {
		return statusText;
	}

	public void setStatusText(String statusText) {
		this.statusText = statusText;
	}

	public Integer getPercentCompleted() {
		return percentCompleted;
	}

	public void setPercentCompleted(Integer percentCompleted) {
		this.percentCompleted = percentCompleted;
	}

	public Collection<ScanUrlCollection> getListOfUrls() {
		return listOfUrls;
	}

	public void setListOfUrls(Collection<ScanUrlCollection> listOfUrls) {
		this.listOfUrls = listOfUrls;
	}

	public Collection<ScanCookieCollection> getListOfCookies() {
		return listOfCookies;
	}

	public void setListOfCookies(Collection<ScanCookieCollection> listOfCookies) {
		this.listOfCookies = listOfCookies;
	}

	public Collection<PluginTest> getTests() {
		return tests;
	}

	public void setTests(Collection<PluginTest> tests) {
		this.tests = tests;
	}

	public Collection<ScanRecordAbstract> getScanRecords() {
		return scanRecords;
	}

	public void setScanRecords(Collection<ScanRecordAbstract> scanRecords) {
		this.scanRecords = scanRecords;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getActiveScanRecordId() {
		return activeScanRecordId;
	}

	public void setActiveScanRecordId(Integer activeScanRecordId) {
		this.activeScanRecordId = activeScanRecordId;
	}
	
	
	
}
