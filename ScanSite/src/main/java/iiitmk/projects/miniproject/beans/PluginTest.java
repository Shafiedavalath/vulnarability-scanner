package iiitmk.projects.miniproject.beans;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="plugin_tests")
public class PluginTest {
	@Id
	@Column(name="test_id")
	private String testId;
	
	@Column(name="test_seq", nullable = false)
	private int testSeq;
	
	@Column(name="name", nullable = false)
	private String name;
	
	@Column(name="state", nullable = false)
	private String state;
	
	@Column(name="is_enabled")
	private Boolean isEnabled;
	
	@Column(name="arguments")
	private String arguments;
	
	@Column(name="success_condition")
	private String successCondition;
	
	@Column(name="condition_type")
	private String successConditionType;
	
	@Column(name="require_parameterized_url")
	private Boolean requireParameterizedUrl;
	
	@Column(name="report_path")
	private String reportPath;
	
	@Column(name="severity")
	private String severity;
	
	@Column(name="description")
	private String description;
	
	@Column(name="solution")
	private String solution;
	
	@Column(name="impact")
	private String impact;
	
	@Column(name="reference")
	private String reference;
	
	@ManyToOne
	@JoinColumn(name="plugin_id")
	private Plugin plugin;
	
	@ManyToMany(mappedBy="tests")
	private Collection<ScanSettings> scanSettings = new ArrayList<ScanSettings>();
	
	@OneToMany(mappedBy = "test")
	private Collection<ScanRecordDetailed> scanRecords = new ArrayList<ScanRecordDetailed>();

	@ManyToMany(mappedBy = "tests")
	private Collection<ScanRecordAbstract> tests = new ArrayList<ScanRecordAbstract>();

	public String getTestId() {
		return testId;
	}

	public void setTestId(String testId) {
		this.testId = testId;
	}

	public int getTestSeq() {
		return testSeq;
	}

	public void setTestSeq(int testSeq) {
		this.testSeq = testSeq;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Boolean getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(Boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public Plugin getPlugin() {
		return plugin;
	}

	public void setPlugin(Plugin plugin) {
		this.plugin = plugin;
	}

	public Collection<ScanSettings> getScanSettings() {
		return scanSettings;
	}

	public void setScanSettings(Collection<ScanSettings> scanSettings) {
		this.scanSettings = scanSettings;
	}

	public Collection<ScanRecordDetailed> getScanRecords() {
		return scanRecords;
	}

	public void setScanRecords(Collection<ScanRecordDetailed> scanRecords) {
		this.scanRecords = scanRecords;
	}

	public Collection<ScanRecordAbstract> getTests() {
		return tests;
	}

	public void setTests(Collection<ScanRecordAbstract> tests) {
		this.tests = tests;
	}

	public String getArguments() {
		return arguments;
	}

	public void setArguments(String arguments) {
		this.arguments = arguments;
	}

	public String getSuccessCondition() {
		return successCondition;
	}

	public void setSuccessCondition(String successCondition) {
		this.successCondition = successCondition;
	}

	public String getSuccessConditionType() {
		return successConditionType;
	}

	public void setSuccessConditionType(String successConditionType) {
		this.successConditionType = successConditionType;
	}

	public Boolean getRequireParameterizedUrl() {
		return requireParameterizedUrl;
	}

	public void setRequireParameterizedUrl(Boolean requireParameterizedUrl) {
		this.requireParameterizedUrl = requireParameterizedUrl;
	}

	public String getReportPath() {
		return reportPath;
	}

	public void setReportPath(String reportPath) {
		this.reportPath = reportPath;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	
	
	
}
