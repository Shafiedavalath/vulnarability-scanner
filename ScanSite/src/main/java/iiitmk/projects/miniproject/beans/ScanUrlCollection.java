package iiitmk.projects.miniproject.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="scan_url_collection")
public class ScanUrlCollection {	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="mapping_id")
	private Integer mappingId;
	
	@Column(name="url")
	private String url;
	
	@Column(name="has_parameters")
	private Boolean hasParameter;
	
	@Column(name="is_selected")
	private Boolean isSelected;
	
	@Column(name="created_on")
	private Date createdOn;
	
	@ManyToOne
	@JoinColumn(name="scan_id")
	private ScanSettings scanSettings;	

	@OneToMany(mappedBy = "url")
	private Collection<ScanRecordDetailed> scanRecords = new ArrayList<ScanRecordDetailed>();
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	


	public Boolean getHasParameter() {
		return hasParameter;
	}

	public void setHasParameter(Boolean hasParameter) {
		this.hasParameter = hasParameter;
	}

	public Boolean getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getMappingId() {
		return mappingId;
	}

	public void setMappingId(Integer mappingId) {
		this.mappingId = mappingId;
	}

	public ScanSettings getScanSettings() {
		return scanSettings;
	}

	public void setScanSettings(ScanSettings scanSettings) {
		this.scanSettings = scanSettings;
	}

	public Collection<ScanRecordDetailed> getScanRecords() {
		return scanRecords;
	}

	public void setScanRecords(Collection<ScanRecordDetailed> scanRecords) {
		this.scanRecords = scanRecords;
	}
	@Override
	public boolean equals(Object obj){
        if (obj instanceof ScanUrlCollection) {
        	ScanUrlCollection pp = (ScanUrlCollection) obj;
            return (pp.getUrl().equals(this.getUrl()));
        } else {
            return false;
        }
    }
	@Override
	public int hashCode(){
        return url==null?0:url.hashCode();
    }
}
