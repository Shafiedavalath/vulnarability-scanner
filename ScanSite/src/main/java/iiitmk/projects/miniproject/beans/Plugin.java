package iiitmk.projects.miniproject.beans;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "plugins")
public class Plugin {
	@Id
	@Column(name = "plugin_id")
	private String pluginId;

	@Column(name = "plugin_seq")
	private int pluginSeq;

	@Column(name = "name")
	private String name;
	
	@Column(name = "execution_path")
	private String executionPath;
	
	@OneToMany(mappedBy = "plugin", cascade = CascadeType.ALL)
	private Collection<PluginTest> listOfTests = new ArrayList<PluginTest>();

	public String getPluginId() {
		return pluginId;
	}

	public void setPluginId(String pluginId) {
		this.pluginId = pluginId;
	}

	public int getPluginSeq() {
		return pluginSeq;
	}

	public void setPluginSeq(int pluginSeq) {
		this.pluginSeq = pluginSeq;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<PluginTest> getListOfTests() {
		return listOfTests;
	}

	public void setListOfTests(Collection<PluginTest> listOfTests) {
		this.listOfTests = listOfTests;
	}

	public String getExecutionPath() {
		return executionPath;
	}

	public void setExecutionPath(String executionPath) {
		this.executionPath = executionPath;
	}
	
	
}
