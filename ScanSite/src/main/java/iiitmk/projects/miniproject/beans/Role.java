package iiitmk.projects.miniproject.beans;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "roles")
public class Role {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "role_id")
	private int roleId;

	@Column(name = "type")
	private String type;

	@OneToMany(fetch = FetchType.EAGER,mappedBy = "role")
	private Collection<UserRoleMapping> userRoles = new ArrayList<UserRoleMapping>();

	@ManyToMany(mappedBy = "roles")
	private Collection<User> users = new ArrayList<User>();

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Collection<UserRoleMapping> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Collection<UserRoleMapping> userRoles) {
		this.userRoles = userRoles;
	}

	public Collection<User> getUsers() {
		return users;
	}

	public void setUsers(Collection<User> users) {
		this.users = users;
	}	
}
