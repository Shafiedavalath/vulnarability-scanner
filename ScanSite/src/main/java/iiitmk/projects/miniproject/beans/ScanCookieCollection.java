package iiitmk.projects.miniproject.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="scan_cookie_collection")
public class ScanCookieCollection {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="mapping_id")
	private String mappingId;
	
	@Column(name = "cookie_name")
	private String cookieName;

	@Column(name = "cookie_value")
	private String cookieValue;
	
	@ManyToOne
	@JoinColumn(name="scan_id")
	private ScanSettings scanSettings;

	public String getCookieName() {
		return cookieName;
	}

	public void setCookieName(String cookieName) {
		this.cookieName = cookieName;
	}

	public String getCookieVallue() {
		return cookieValue;
	}

	public void setCookieVallue(String cookieVallue) {
		this.cookieValue = cookieVallue;
	}

	public String getMappingId() {
		return mappingId;
	}

	public void setMappingId(String mappingId) {
		this.mappingId = mappingId;
	}

	public String getCookieValue() {
		return cookieValue;
	}

	public void setCookieValue(String cookieValue) {
		this.cookieValue = cookieValue;
	}

	public ScanSettings getScanSettings() {
		return scanSettings;
	}

	public void setScanSettings(ScanSettings scanSettings) {
		this.scanSettings = scanSettings;
	}

}
