var currentStep = 0;
var scanMode = 0; // 0-> not yet chosen, 1-> Full, 2-> Custom
var scanJson = {};

var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
var csrfToken = $("meta[name='_csrf']").attr("content");
var csrfHeader = $("meta[name='_csrf_header']").attr("content");

function updateScanJson() {
	if (currentStep == 0) {
		scanJson.primaryUrl = $("input[name='primaryUrlTextBox']").val();
	}
	if (currentStep == 1) {
		if (scanMode == 1) {
			scanJson.scanType = "full";
		} else if (scanMode == 2) {
			scanJson.scanType = "custom";
		}
	}
	if (currentStep == 2) {
		scanJson.listOfUrls = [];
		$.each($("#listOfurls option:selected"), function(i, child) {
			scanJson.listOfUrls[i] = {
				"url" : $(child).val(),
				"hasParameter" : $(child).attr("hasParam")
			};
		});
	}
	if (currentStep == 3) {
		scanJson.tests = [];
		$.each($("#listOftests").val(), function(i, val) {
			scanJson.tests[i] = {
				"testId" : val
			};
		});
	}
}

function createScan() {

	// $("#btn-save").prop("disabled", true);

	scanJson[csrfParameter] = csrfToken;

	var headers = {};
	headers[csrfHeader] = csrfToken;

	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : createScanUrl,
		headers : headers,
		data : JSON.stringify(scanJson),
		dataType : 'html',
		timeout : 100000,
		success : function(data) {
			console.log("SUCCESS: ", data);
			$("#scan-now-button").attr("href", startScanUrl+ data);
		},
		error : function(e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function crawlUrls() {

	var primaryUrl = {};
	primaryUrl.primaryUrl = $("input[name='primaryUrlTextBox']").val();
	primaryUrl[csrfParameter] = csrfToken;

	var headers = {};
	headers[csrfHeader] = csrfToken;
	spinner("show");
	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : crawlUrl,
		headers : headers,
		data : JSON.stringify(primaryUrl),
		dataType : 'json',
		timeout : 900000,
		success : function(data) {
			console.log("SUCCESS: ", data);
			populateUrlBox(data);
			spinner("hide");
		},
		error : function(e) {
			console.log("ERROR: ", e);
			spinner("hide");
		},
		done : function(e) {
			console.log("DONE");
			spinner("hide");
		}
	});
}

function spinner(mode) {
	if (mode == 'show') {
		$data = {
			autoCheck : 32,
			size : 1,
			bgColor : "#FFF",
			bgOpacity : 0.5,
			fontColor : "#000",
			title : "Crawling pages, please wait...",
			isOnly : true
		};
		$.loader.open($data);
	} else if (mode == 'hide') {
		$.loader.close(true);
	}
}

function populateUrlBox(jsonArray) {
	for ( var key in jsonArray) {
		if (jsonArray.hasOwnProperty(key)) {
			$("#listOfurls").append(
					"<option value='" + jsonArray[key].url + "' hasParam='"
							+ jsonArray[key].hasParameter + "'>"
							+ jsonArray[key].url + "</option>");
		}
	}
}

$(document)
		.ready(
				function() {
					console.log("ready new scan.js!");
					$("#back-button").addClass("hide");

					$("#next-button")
							.on(
									'click ',
									function(e) {

										switch (currentStep) {
										case 0:
											$("#back-button").removeClass(
													"hide");
											$("#stage-1-div").addClass("hide");
											$("#stage-2-div").removeClass(
													"hide");
											$("#stage-3-div").addClass("hide");
											$("#stage-4-div").addClass("hide");
											$("#stage-5-div").addClass("hide");
											$("#progress-bar-div")
													.html(
															"<span class='progress-meter' style='width: 40%'>"
																	+ "<p class='progress-meter-text'>Choose Scan Mode</p></span>");
											updateScanJson();

											currentStep = 1;
											break;
										case 1:
											if (scanMode == 0) {
												alert("Please select scan mode!");
											}
											if (scanMode == 1) {
												$("#stage-1-div").addClass(
														"hide");
												$("#stage-2-div").addClass(
														"hide");
												$("#stage-3-div").addClass(
														"hide");
												$("#stage-4-div").removeClass(
														"hide");
												$("#stage-5-div").addClass(
														"hide");
												$("#progress-bar-div")
														.html(
																"<span class='progress-meter' style='width: 80%'>"
																		+ "<p class='progress-meter-text'>Choose Tests</p></span>");
												updateScanJson();
												currentStep = 3;
											} else if (scanMode == 2) {
												$("#stage-1-div").addClass(
														"hide");
												$("#stage-2-div").addClass(
														"hide");
												$("#stage-3-div").removeClass(
														"hide");
												$("#stage-4-div").addClass(
														"hide");
												$("#stage-5-div").addClass(
														"hide");
												$("#progress-bar-div")
														.html(
																"<span class='progress-meter' style='width: 60%'>"
																		+ "<p class='progress-meter-text'>Choose Pages</p></span>");
												updateScanJson();
												crawlUrls();
												currentStep = 2;
											}

											break;
										case 3:
											$("#stage-1-div").addClass("hide");
											$("#stage-2-div").addClass("hide");
											$("#stage-3-div").addClass("hide");
											$("#stage-4-div").addClass("hide");
											$("#stage-5-div").removeClass(
													"hide");
											$("#next-button").addClass("hide");
											$("#back-button").addClass("hide");
											$("#progress-bar-div")
													.html(
															"<span class='progress-meter' style='width: 100%'>"
																	+ "<p class='progress-meter-text'>Start Scan</p></span>");
											updateScanJson();
											createScan();
											currentStep = 4;
											break;
										case 2:
											$("#stage-1-div").addClass("hide");
											$("#stage-2-div").addClass("hide");
											$("#stage-3-div").addClass("hide");
											$("#stage-4-div").removeClass(
													"hide");
											$("#stage-5-div").addClass("hide");
											$("#progress-bar-div")
													.html(
															"<span class='progress-meter' style='width: 80%'>"
																	+ "<p class='progress-meter-text'>Choose Tests</p></span>");
											$("#next-button").text("Finish");
											$("#next-button").addClass("success");
											updateScanJson();
											currentStep = 3;
											break;

										default:
											break;
										}

									});

					$("#full-scan-button").on('click ', function(e) {
						scanMode = 1;
					});
					$("#custom-scan-button").on('click ', function(e) {
						scanMode = 2;
					});

				});