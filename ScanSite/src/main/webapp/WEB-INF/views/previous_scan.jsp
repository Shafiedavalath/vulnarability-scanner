<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Site Scan - Home</title>
<link rel="stylesheet"
	href="<c:url value = "/static/css/foundation.css" />" />
<!-- the app.css file must be used to write custom css. Should not make any modifications in any other css files  -->
<link rel="stylesheet" href="<c:url value = "/static/css/app.css" />" />
<link href="<c:url value = "/static/css/font-awesome.min.css" />"
	rel="stylesheet" type='text/css'>
<link href='<c:url value = "/static/css/foundation-icons.css" />'
	rel='stylesheet' type='text/css'>
<c:url var="startScanUrl" value="/startScan/" />
<script type="text/javascript">
	var focuzzedScanId = "${scanId}";
</script>
</head>
<body>
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
			<div class="off-canvas position-left reveal-for-large"
				id="offCanvasLeft" data-off-canvas>
				<div class="row">
					<!-- include the html written in sideBar.jsp -->
					<jsp:include page="sideBar.jsp" />
				</div>
			</div>
			<div class="off-canvas-content" data-off-canvas-content>
				<!-- include the html written in header.jsp -->
				<jsp:include page="header.jsp"></jsp:include>



				<!-- add the html for new pages here-->
				<!-- delete the sample html and add your own -->
				<!-- sample html starts -->
				<div class="row">

					<div class="small-12 large-12 columns">
						<h5>${scanDetails.primaryUrl}</h5>
					</div>
				</div>
				<div class="row">

					<div class="small-6 large-6 columns">
						<div class="row">
							<div class="large-12 columns">
								<div class="callout">
									<form action="${startScanUrl}${scanId}">
										<input class="button success float-center" type="submit" value="Scan Now"/>
									</form>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="large-12 columns">
								<div class="callout">
									<h6>Tests</h6>
									<select id="listOftests" multiple>
										<c:forEach var="test" items="${scanDetails.tests}">
											<option value="${test.testId}">${test.name}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>

					</div>
					<div class="small-6 large-6 columns">

						<div class="row">
							<fieldset class="large-12 columns">
								<div class="callout">
									<legend>Scan Type</legend>

									<c:if test='${scanDetails.scanType == "full"}'>
										<input type="radio" name="full" value="Red" id="fullMode"
											required checked="checked">
										<label for="fullMode">Full</label>
										<input type="radio" name="custom" value="Blue" id="customMode">
										<label for="customMode">Custom</label>
									</c:if>
									<c:if test='${scanDetails.scanType == "custom"}'>
										<input type="radio" name="full" value="Red" id="fullMode"
											required>
										<label for="fullMode">Full</label>
										<input type="radio" name="custom" value="Blue" id="customMode"
											checked="checked">
										<label for="customMode">Custom</label>
									</c:if>

								</div>
							</fieldset>
						</div>

						<div class="row">
							<div class="large-12 columns">
								<div class="callout">
									<h6>Pages</h6>
									<select id="listOfurls" multiple>
										<c:forEach var="url" items="${scanDetails.listOfUrls}">
											<option value="${url.mappingId}">${url.url}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>

						<div class="row">

							<div class="columns large-6">
								<div class="callout">
									<h6>User Agent</h6>
									<input type="text" placeholder="Enter user agent string"
										value="${scanDetails.userAgent}" /> <input type="submit"
										class="button float-center" value="Save" />
								</div>
							</div>
							<div class="columns large-6">
								<div class="callout">
									<h6>Delete Scan</h6>
									<button type="button" class="alert button float-center">Delete</button>
								</div>
							</div>
							<!-- <div class="small-6 large-6 columns">
											<div class="callout">

												<table>
													<thead>
														<tr>
															<th>Cookies</th>


														</tr>
													</thead>
													<tbody>
														<tr>
															<td>Name</td>
															<td>Value</td>
															<td>Action</td>


														</tr>
														<tr>
															<td>Auth</td>
															<td>Something</td>
															<td>

																<button type="button" class="alert tiny button">Delete</button>
															</td>

														</tr>
														</tr>
														<tr>
															<td><input type="text"
																placeholder="Right-aligned text input"></td>
															<td><input type="text"
																placeholder="Right-aligned text input"></td>
															<td><button type="submit" class="tiny button">Add</button></td>


														</tr>
													</tbody>
												</table>

											</div>

										</div> -->
						</div>
						<%-- <div class="row">
							<div class="small-12 large-12 columns">
								<div class="callout">
									<div class="row">

										<div class="small-6 large-6 columns">

											<div class="callout">
												<div class="row">
													<div class="small-12 large-12 columns">
														<div class="callout">
															User Agent
															<table>
																<thead>
																	<tr>
																<tbody>
																	<tr>
																		<td><input type="text"
																			value="${scanDetails.userAgent}"
																			placeholder=" text input"></td>
																		<td><input type="text" placeholder="text input"></td>
																		<td><button type="submit" class="tiny button">Change</button></td>



																	</tr>
																</tbody>
																</tr>
																</thead>
															</table>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="small-12 large-12 columns">
														<div class="callout">
															<table>
																<thead>
																	<tr>
																		<th>Report Scan</th>
																<tbody>
																	<tr>
																		<td><center>
																				
																			</center></td>
																		</td>
																	</tr>
																</tbody>
																</tr>
																</thead>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div> --%>
					</div>
				</div>




				<!-- include the html written in footer.jsp-->
				<jsp:include page="footer.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script src="<c:url value = "/static/js/vendor/jquery.min.js" />"></script>
	<script src="<c:url value = "/static/js/vendor/what-input.min.js" />"></script>
	<script src="<c:url value = "/static/js/foundation.min.js" />"></script>
	<script src="<c:url value = "/static/js/app.js" />"></script>
</body>
</html>