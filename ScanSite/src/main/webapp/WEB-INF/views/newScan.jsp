<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<title>Site Scan - Home</title>
<link rel="stylesheet" href="static/css/foundation.css" />
<!-- the app.css file must be used to write custom css. Should not make any modifications in any other css files  -->
<link rel="stylesheet" href="static/css/app.css" />
<link href="static/css/font-awesome.min.css" rel="stylesheet"
	type='text/css'>
<link href='static/css/foundation-icons.css' rel='stylesheet'
	type='text/css'>
<c:url var="createScanUrl" value="/insertNewScan" />
<c:url var="crawlUrl" value="/crawlUrls" />
<c:url var="startScanUrl" value="/startScan/" />
<script type="text/javascript">
	var focuzzedScanId = null;
	var createScanUrl = "${createScanUrl}";
	var startScanUrl = "${startScanUrl}";
	var crawlUrl = "${crawlUrl}";
	var csrfToken = "${_csrf.token}";
</script>
</head>
<body>

	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
			<div class="off-canvas position-left reveal-for-large"
				id="offCanvasLeft" data-off-canvas>
				<div class="row">
					<!-- include the html written in sideBar.jsp -->
					<jsp:include page="sideBar.jsp" />
				</div>
			</div>
			<div class="off-canvas-content" data-off-canvas-content>
				<!-- include the html written in header.jsp -->
				<jsp:include page="header.jsp"></jsp:include>



				<!-- add the html for new pages here-->
				<!-- delete the sample html and add your own -->
				<!-- sample html starts -->
				<div class="row">
					<div class="columns large-12">
						<div class="row">
							<div class="columns large-12">
								<div id="progress-bar-div" class="progress" role="progressbar"
									tabindex="0" aria-valuenow="20" aria-valuemin="0"
									aria-valuetext="25 percent" aria-valuemax="100">
									<span class="progress-meter" style="width: 20%">
										<p class="progress-meter-text">Enter Domain</p>
									</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="columns large-12 new-scan-middle">
								<div class="row" id="stage-1-div">
									<div class="columns large-4 large-offset-4">
										<input class="primary-url-textbox" name="primaryUrlTextBox"
											type="text" placeholder="Enter domain or IP Address">
									</div>
								</div>
								<div class="row hide" id="stage-2-div">
									<div class="columns large-4 large-offset-4">
										<div data-grouptype="OR" class="button-group">
											<button id="full-scan-button" href="#"
												class="small button primary radius">&nbsp;&nbsp;&nbsp;Full
												Scan&nbsp;&nbsp;&nbsp;&nbsp;</button>
											<button id="custom-scan-button" href="#"
												class="small button success radius">&nbsp;&nbsp;Custom
												Scan</button>
										</div>
									</div>
								</div>
								<div class="row hide" id="stage-3-div">
									<div class="columns large-6 large-offset-3">
										<select id="listOfurls" multiple>

										</select>
									</div>
								</div>
								<div class="row hide" id="stage-4-div">
									<div class="columns large-4 large-offset-4">
										<select id="listOftests" multiple>
											<c:forEach var="test" items="${listOfTests}">
												<option value="${test.testId}">${test.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="row hide" id="stage-5-div">
									<div class="columns large-4 large-offset-5">
										<a id="scan-now-button" class="button large success" href="#">Scan
											Now</a>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="columns large-12">
								<a id="next-button" class="small button float-right" href="#">Next</a>
								<a id="back-button" class="small button float-right" href="#">Back</a>

							</div>
						</div>
					</div>
				</div>
				<!-- sample html ends -->



				<!-- include the html written in footer.jsp-->
				<jsp:include page="footer.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script src="static/js/vendor/jquery.min.js"></script>
	<script src="static/js/vendor/what-input.min.js"></script>
	<script src="static/js/foundation.min.js"></script>
	<script src="static/js/app.js"></script>
	<script src="static/js/jquery-loader.js"></script>
	<script src="static/js/newscan.js"></script>
</body>
</html>