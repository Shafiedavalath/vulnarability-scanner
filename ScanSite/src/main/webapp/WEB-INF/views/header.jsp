<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="small-12 large-12 columns">	
		<a class="small button float-right" href="<c:url value="/logout" />">Logout</a>
	</div>
</div>