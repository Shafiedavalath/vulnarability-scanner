<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Site Scan - Home</title>
<link rel="stylesheet"
	href="<c:url value = "/static/css/foundation.css" />" />
<!-- the app.css file must be used to write custom css. Should not make any modifications in any other css files  -->
<link rel="stylesheet" href="<c:url value = "/static/css/app.css" />" />
<link href="<c:url value = "/static/css/font-awesome.min.css" />"
	rel="stylesheet" type='text/css'>
<link href='<c:url value = "/static/css/foundation-icons.css" />'
	rel='stylesheet' type='text/css'>
<script type="text/javascript">
	var focuzzedScanId = "${scanId}";
</script>
<c:url var="reportsUrl" value="/scanReport/"/>
</head>
<body>
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
			<div class="off-canvas position-left reveal-for-large"
				id="offCanvasLeft" data-off-canvas>
				<div class="row">
					<!-- include the html written in sideBar.jsp -->
					<jsp:include page="sideBar.jsp" />
				</div>
			</div>
			<div class="off-canvas-content" data-off-canvas-content>
				<!-- include the html written in header.jsp -->
				<jsp:include page="header.jsp"></jsp:include>



				<!-- add the html for new pages here-->
				<!-- delete the sample html and add your own -->
				<!-- sample html starts -->
				<div class="row">

					<div class="small-12 large-12 columns">

						<a href="#" class="button">Back</a>

						<h5>Previous Reports</h5>
					</div>
				</div>
				<div class="row">

					<div class="small-12 large-12 columns text-center">
						<div class="callout end">
							<table>
								<thead>
									<tr>
										<th width="150"><center>Date</center></th>
										<th><center>Result</center></th>
										<th width="180"><center>Action</center></th>

									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${abstractRecords != null}">

											<c:forEach var="record" items="${abstractRecords}">
												<tr>
													<td>${record.scannedOn}</td>
													<td><span class="secondary badge">${record.countLow}</span>
														<span class="warning badge">${record.countMedium}</span> <span
														class="alert badge">${record.countHigh}</span></td>
													<td><a href="${reportsUrl}${record.scanRecordId}" class="button">View</a>
														<button type="button" class="alert button">Delete</button></td>
												</tr>

											</c:forEach>

										</c:when>

										<c:otherwise>
											<tr>No records yet!
											</tr>
										</c:otherwise>

									</c:choose>
								</tbody>
							</table>

						</div>





					</div>



				</div>



				<!-- include the html written in footer.jsp-->
				<jsp:include page="footer.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script src="<c:url value = "/static/js/vendor/jquery.min.js" />"></script>
	<script src="<c:url value = "/static/js/vendor/what-input.min.js" />"></script>
	<script src="<c:url value = "/static/js/foundation.min.js" />"></script>
	<script src="<c:url value = "/static/js/app.js" />"></script>
</body>
</html>