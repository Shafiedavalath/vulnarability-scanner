<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Plugins</title>
<link rel="stylesheet" href="static/css/foundation.css" />
<!-- the app.css file must be used to write custom css. Should not make any modifications in any other css files  -->
<link rel="stylesheet" href="static/css/app.css" />
<link href="static/css/font-awesome.min.css" rel="stylesheet"
	type='text/css'>
<link href='static/css/foundation-icons.css' rel='stylesheet'
	type='text/css'>
</head>
<body>
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
			<div class="off-canvas position-left reveal-for-large"
				id="offCanvasLeft" data-off-canvas>
				<div class="row">
					<!-- include the html written in sideBar.jsp -->
					<jsp:include page="sideBar.jsp" />
				</div>
			</div>
			<div class="off-canvas-content" data-off-canvas-content>
				<!-- include the html written in header.jsp -->
				<jsp:include page="header.jsp"></jsp:include>



				<!-- add the html for new pages here-->
				<div class="row">
					<div class="small-12 large-12 columns">
						<h4>Manage Plugins</h4>
					</div>
				</div>
				<div class="row">
					<div class="small-12 large-12 columns">
						<div class="row">
							<div class="small-10 large-10 columns">
								<div class="callout">
									<h6>Eneble/Disable plugins for all users</h6>
									<table class="hover">
										<thead>
											<tr>
												<th width="120">Name</th>
												<th width="150">Type</th>
												<th width="120">Status</th>
												<th width="120">Enable</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>SQL Injection Test-1</td>
												<td>SQL Injection</td>
												<td>Functional</td>
												<td><input id="checkbox1" type="checkbox"></td>
											</tr>
											<tr>
												<td>SQL Injection Test-2</td>
												<td>SQL Injection</td>
												<td>Functional</td>
												<td><input id="checkbox1" type="checkbox"></td>
											</tr>
											<tr>
												<td>XSS Test-1</td>
												<td>XSS</td>
												<td><label></label>New
													<button type="button" class="tiny button">Test</button></td>
												<td><input id="checkbox1" type="checkbox"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- sample html ends -->



				<!-- include the html written in footer.jsp-->
				<jsp:include page="footer.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script src="static/js/vendor/jquery.min.js"></script>
	<script src="static/js/vendor/what-input.min.js"></script>
	<script src="static/js/foundation.min.js"></script>
	<script src="static/js/app.js"></script>
</body>
</html>