<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Site Scan - Home</title>
<link rel="stylesheet" href="static/css/foundation.css" />
<!-- the app.css file must be used to write custom css. Should not make any modifications in any other css files  -->
<link rel="stylesheet" href="static/css/app.css" />
<link href="static/css/font-awesome.min.css" rel="stylesheet"
	type='text/css'>
<link href='static/css/foundation-icons.css' rel='stylesheet'
	type='text/css'>
</head>
<body>
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
			<div class="off-canvas position-left reveal-for-large"
				id="offCanvasLeft" data-off-canvas>
				<div class="row">
					<!-- include the html written in sideBar.jsp -->
					<jsp:include page="sideBar.jsp" />
				</div>
			</div>
			<div class="off-canvas-content" data-off-canvas-content>
				<!-- include the html written in header.jsp -->
				<jsp:include page="header.jsp"></jsp:include>



					<!-- add the html for new pages here-->
					<!-- delete the sample html and add your own -->
					<!-- sample html starts -->
					<div class="row">
						<div class="small-3 large-3 columns">
							<div class="callout">
								<h5>This is a callout.</h5>
								<p>It has an easy to override visual style, and is
									appropriately subdued.</p>
								<a href="#">It's dangerous to go alone, take this.</a>
							</div>
						</div>
						<div class="small-3 large-3 columns">
							<div class="callout">
								<h5>This is a callout.</h5>
								<p>It has an easy to override visual style, and is
									appropriately subdued.</p>
								<a href="#">It's dangerous to go alone, take this.</a>
							</div>
						</div>
						<div class="small-3 large-3 columns">
							<div class="callout">
								<h5>This is a callout.</h5>
								<p>It has an easy to override visual style, and is
									appropriately subdued.</p>
								<a href="#">It's dangerous to go alone, take this.</a>
							</div>
						</div>
						<div class="small-3 large-3 columns">
							<div class="callout">
								<h5>This is a callout.</h5>
								<p>It has an easy to override visual style, and is
									appropriately subdued.</p>
								<a href="#">It's dangerous to go alone, take this.</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="small-3 large-3 columns">
							<div class="callout">
								<h5>This is a callout.</h5>
								<p>It has an easy to override visual style, and is
									appropriately subdued.</p>
								<a href="#">It's dangerous to go alone, take this.</a>
							</div>
						</div>
						<div class="small-3 large-3 columns">
							<div class="callout">
								<h5>This is a callout.</h5>
								<p>It has an easy to override visual style, and is
									appropriately subdued.</p>
								<a href="#">It's dangerous to go alone, take this.</a>
							</div>
						</div>
						<div class="small-3 large-3 columns">
							<div class="callout">
								<h5>This is a callout.</h5>
								<p>It has an easy to override visual style, and is
									appropriately subdued.</p>
								<a href="#">It's dangerous to go alone, take this.</a>
							</div>
						</div>
						<div class="small-3 large-3 columns">
							<div class="callout">
								<h5>This is a callout.</h5>
								<p>It has an easy to override visual style, and is
									appropriately subdued.</p>
								<a href="#">It's dangerous to go alone, take this.</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="small-3 large-3 columns">
							<div class="callout">
								<h5>This is a callout.</h5>
								<p>It has an easy to override visual style, and is
									appropriately subdued.</p>
								<a href="#">It's dangerous to go alone, take this.</a>
							</div>
						</div>
						<div class="small-3 large-3 columns">
							<div class="callout">
								<h5>This is a callout.</h5>
								<p>It has an easy to override visual style, and is
									appropriately subdued.</p>
								<a href="#">It's dangerous to go alone, take this.</a>
							</div>
						</div>
						<div class="small-3 large-3 columns">
							<div class="callout">
								<h5>This is a callout.</h5>
								<p>It has an easy to override visual style, and is
									appropriately subdued.</p>
								<a href="#">It's dangerous to go alone, take this.</a>
							</div>
						</div>
						<div class="small-3 large-3 columns">
							<div class="callout">
								<h5>This is a callout.</h5>
								<p>It has an easy to override visual style, and is
									appropriately subdued.</p>
								<a href="#">It's dangerous to go alone, take this.</a>
							</div>
						</div>
					</div>
					<!-- sample html ends -->
					
					
					
				<!-- include the html written in footer.jsp-->
				<jsp:include page="footer.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script src="static/js/vendor/jquery.min.js"></script>
	<script src="static/js/vendor/what-input.min.js"></script>
	<script src="static/js/foundation.min.js"></script>
	<script src="static/js/app.js"></script>
</body>
</html>