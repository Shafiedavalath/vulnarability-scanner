<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Site Scan - Home</title>
<link rel="stylesheet" href="static/css/foundation.css" />
<!-- the app.css file must be used to write custom css. Should not make any modifications in any other css files  -->
<link rel="stylesheet" href="static/css/app.css" />
<link href="static/css/font-awesome.min.css" rel="stylesheet"
	type='text/css'>
<link href='static/css/foundation-icons.css' rel='stylesheet'
	type='text/css'>
<script type="text/javascript">
	var focuzzedScanId = "${scanId}";
</script>
</head>
<body>
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
			<div class="off-canvas position-left reveal-for-large"
				id="offCanvasLeft" data-off-canvas>
				<div class="row">
					<!-- include the html written in sideBar.jsp -->
					<jsp:include page="sideBar.jsp" />
				</div>
			</div>
			<div class="off-canvas-content" data-off-canvas-content>
				<!-- include the html written in header.jsp -->
				<jsp:include page="header.jsp"></jsp:include>



				<!-- add the html for new pages here-->
				<!-- delete the sample html and add your own -->
				<!-- sample html starts -->
				<c:if test="${scanId != null}">
					<div class="row">
						<div class="small-12 large-12 columns">
							<h3>Scaning in Progress...</h3>
							<p>
								<b>Domain: </b> ${activeScan.primaryUrl}
							</p>
						</div>
					</div>
					<div class="row">

						<div class="small-6 large-6 columns">
							<div class="callout ">

								<a href="#" class="button alert tiny float-right stop button">Stop</a>
								<h6>Status</h6>
								<div id="progress-bar-div" class="progress" role="progressbar"
									tabindex="0" aria-valuenow="20" aria-valuemin="0"
									aria-valuetext="25 percent" aria-valuemax="100">
									<span class="progress-meter " style="width: ${activeScan.percentCompleted}%">
										<p class="progress-meter-text scan-status-bar">${activeScan.statusText}</p>
									</span>
								</div>


							</div>
						</div>
						<div class="small-6 large-6 columns end">
							<div class="callout ">
								<h6>Reported So far</h6>
								<div class="row">
									<div class="small-12 large-12 columns">
										<div class="small-12 large-12 columns">
											<div class="row">

												<div class="large-12 columns">

													<button href="#" class="alert hollow button large float-center">${scanRecord.countHigh}<br>High<br></button>

												</div>

											</div>											
										</div>
										<div class="row">
											<div class="large-6 columns">
												
												<button href="#" class="warning hollow button large float-center">${scanRecord.countMedium}<br>Medium<br></button>
									
											</div>
											<div class="large-6 columns">
												
												<button href="#" class="secondary hollow button large float-center">${scanRecord.countLow}<br>Low<br></button>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</c:if>
				<c:if test="${scanId == null}">
					<div class="row">
						<div class="small-12 large-12 columns">
							<h4>All scans are completed!</h4>
						</div>
					</div>
				</c:if>
				<!-- sample html ends -->



				<!-- include the html written in footer.jsp-->
				<jsp:include page="footer.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script src="static/js/vendor/jquery.min.js"></script>
	<script src="static/js/vendor/what-input.min.js"></script>
	<script src="static/js/foundation.min.js"></script>
	<script src="static/js/app.js"></script>
</body>
</html>