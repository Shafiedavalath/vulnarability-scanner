<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Site Scan - Detailed Report</title>
<link rel="stylesheet"
	href="<c:url value = "/static/css/foundation.css" />" />
<!-- the app.css file must be used to write custom css. Should not make any modifications in any other css files  -->
<link rel="stylesheet" href="<c:url value = "/static/css/app.css" />" />
<link href="<c:url value = "/static/css/font-awesome.min.css" />"
	rel="stylesheet" type='text/css'>
<link href='<c:url value = "/static/css/foundation-icons.css" />'
	rel='stylesheet' type='text/css'>
<script type="text/javascript">
	var focuzzedScanId = "${scanId}";
</script>
</head>
<body>
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
			<div class="off-canvas position-left reveal-for-large"
				id="offCanvasLeft" data-off-canvas>
				<div class="row">
					<!-- include the html written in sideBar.jsp -->
					<jsp:include page="sideBar.jsp" />
				</div>
			</div>
			<div class="off-canvas-content" data-off-canvas-content>
				<!-- include the html written in header.jsp -->
				<jsp:include page="header.jsp"></jsp:include>



					<!-- add the html for new pages here-->
					<!-- delete the sample html and add your own -->
					<!-- sample html starts -->
			<div class="row">
                                        
					<div class="small-12 large-12 columns">
					  
                                        <a href="#" class="button">Back</a>
                                            
						<h4>${detailedReport.test.name}</h4>
					</div>
				</div>
				<div class="row">
					
					<div class="small-12 large-12 columns">
					  <div class="callout">
					      <div class="row">
					              <div class="small-12 large-12 columns">
					                <div class="callout">
					                Type: ${detailedReport.test.plugin.name }<br/>
					                Severity: ${detailedReport.severity}<br/>
					                Found at: ${detailedReport.url.url }
					                </div>
					            </div>
					      </div>
					<div class="row">
					              <div class="small-12 large-12 columns">
					                <div class="callout">
					                <b>Description</b><br/>
					                	${detailedReport.test.description }
					                </div>
					            </div>
					      </div>
					<div class="row">
					              <div class="small-12 large-12 columns">
					                <div class="callout">
					                	<b>Impact</b><br/>
					                	${detailedReport.test.impact }
					                </div>
					            </div>
					      </div>
					<div class="row">
					              <div class="small-12 large-12 columns">
					                <div class="callout">
					                <b>Solution</b><br/>
					                	${detailedReport.test.solution }
					                </div>
					            </div>
					      </div>
					<div class="row">
					              <div class="small-12 large-12 columns">
					                <div class="callout">
					                	<b>References</b><br/>
					                	${detailedReport.test.reference }
					                </div>
					            </div>
					      </div>

						

					</div>
					
					</div>
					</div>
					
					

					
					<!-- sample html ends -->
					
					
					
				<!-- include the html written in footer.jsp-->
				<jsp:include page="footer.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script src="<c:url value = "/static/js/vendor/jquery.min.js" />"></script>
	<script src="<c:url value = "/static/js/vendor/what-input.min.js" />"></script>
	<script src="<c:url value = "/static/js/foundation.min.js" />"></script>
	<script src="<c:url value = "/static/js/app.js" />"></script>
</body>
</html>