----------------
PROJECT PROPOSAL
----------------

Application for analyzing vulnerabilities in web applications/web sites
------------------------------------------------------------------------

The proposed application will be based on client server architecture, with a web based GUI. Users are provided with logins, and can enter the URL/IP Address of the web application to be checked for vulnerabilities. After scanning, it will present a detailed report on the vulnerabilities present in the web app ordered by it's severity. The results will be stored in the database for future reference.
Users can select the types of vulnerabilities to be checked for each URL, before starting the scan. 

The application will be build in such a way that modules for new vulnerabilities can be added without changing any code. It will make use of the pluggable web application concept, where each vulnerability check is build as a separate module and can be added or removed on the fly.
The application will leverage various tools readily available in the Kali Linux such as sqlMap and BED.
Plungins for the following vulnerabilities will be build initially:
1. SQL Injection

Intended users
--------------
The intended users can be a single user or an organization. In the case of a single user, one can deploy it locally on his computer and start working with it. In the case of an organization, a central server can be configured to deploy the application, and multiple clients can use it simultaneously.
Future enhancement
An API can be built to be available for public, using which one can check the degree of safety of a website. The API will make use of the past results stored in the database to calculate the safety rating.

The API can be used in various ways, for instance, one can build a browser plugin which gives safety indications about each website the user is visiting.

Group Members
-------------
Ibinas CH, Manoj Rajak, Muhammad Shafi E, Vipin Raj.KP